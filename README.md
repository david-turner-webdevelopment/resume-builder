# resume-builder

The resume-builder project is a spring boot, AngularJS application designed to render a professional profile. Through URI routing, the app is able to host multiple online profiles on a single instance. Users will be able to examine a individual's profesional portfolio, download their resume, and examine any projects if any exist through interacitve template cards. 

#Development
Build: *mvn clean install*

Run: *mvn spring-boot:run*

**Notes:**

This project leverages spring data and JDBC - so a database connection is required for implementation.

This project uses maven as it's build management tool - and because it is a spring boot app - a tomcat server is bundled in the deployable. 



